import Vue from 'vue';
import Vuetify from 'vuetify/lib';

import {VueMasonryPlugin} from 'vue-masonry';

Vue.use(Vuetify);
Vue.use(VueMasonryPlugin)

export default new Vuetify({
});
